using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager instance;
    [SerializeField] private Resource pumpkins;
    [SerializeField] private Resource walls;

    public ResourceE ActiveResource;

    public enum ResourceE
    {
        Pumpkin,
        Wall
    }


    public Dictionary<ResourceE, Resource> resources = new Dictionary<ResourceE, Resource>();

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        resources[ResourceE.Pumpkin] = pumpkins;
        resources[ResourceE.Wall] = walls;

        SetActiveResource(ResourceE.Pumpkin);
    }

    public void SetActiveResource(ResourceE re)
    {
        ActiveResource = re;
        foreach (var item in resources)
        {
            item.Value.SetSelected(re == item.Key);
        }
    }

    public bool TryUseResource() => TryUseResource(ActiveResource);

    public bool TryUseResource(ResourceE resource)
    {
        if (resources[resource].Resources > 0)
        {
            resources[resource].Use();
            return true;
        }
        return false;
    }

    public void AddResource(ResourceE resource, int num)
    {
        resources[resource].Add(num);
    }
}
