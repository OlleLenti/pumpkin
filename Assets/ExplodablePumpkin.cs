using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodablePumpkin : MonoBehaviour
{

    [SerializeField] private List<GameObject> explosionParts;
    [SerializeField] private GameObject removeOnExplosion;
    [SerializeField] private Rigidbody parentRb;
    [SerializeField] private GameObject addRbOnExplosion;
    [SerializeField] private GameObject particleSystemPrefab;
    [SerializeField] private LayerMask zombieLayerMask;

    public void Explode()
    {

        foreach (var part in explosionParts)
        {
            part.SetActive(true);
            part.GetComponent<Rigidbody>().AddExplosionForce(300, transform.position, 5);
        }

        removeOnExplosion.SetActive(false);

        Instantiate(particleSystemPrefab, transform.position, Quaternion.identity);

        //rbRemoveOnExplosion.AddExplosionForce(30000, transform.position, 5);
        Destroy(parentRb);

        addRbOnExplosion.AddComponent<Rigidbody>();

        var zombiesHit = Physics.OverlapSphere(transform.position, 5, zombieLayerMask);

        foreach (var item in zombiesHit)
        {
            Instantiate(particleSystemPrefab, item.transform.position, Quaternion.identity);

            Destroy(item.gameObject);
        }

        Destroy(parentRb.gameObject, 5);
    }
}
