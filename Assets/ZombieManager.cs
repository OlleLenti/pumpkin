using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieManager : MonoBehaviour
{
    [SerializeField] float SpawnRate = 2f;
    [SerializeField] GameObject ZombiePrefab;
    [SerializeField] Transform SpawnLocation;

    [SerializeField] Transform ZombieTarget;

    private float counter = 0;

    void Update()
    {
        counter += Time.deltaTime;

        if (counter > SpawnRate)
        {
            counter -= SpawnRate;
            var zomb = Instantiate(ZombiePrefab, 
                SpawnLocation.transform.position, 
                Quaternion.identity, this.transform);

            zomb.GetComponent<ChaseTransform>().target = ZombieTarget;
        }
    }
}
