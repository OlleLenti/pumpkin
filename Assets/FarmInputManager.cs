using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmInputManager : MonoBehaviour
{
    [SerializeField] private GameObject pumpkinPrefab;
    [SerializeField] private GameObject wallPrefab;
    [SerializeField] private DestroyPumpkinSphere destroyPumpkinSphere;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            UseSelectedResource();

        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            destroyPumpkinSphere.DestroyAll = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ResourceManager.instance.SetActiveResource(ResourceManager.ResourceE.Pumpkin);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ResourceManager.instance.SetActiveResource(ResourceManager.ResourceE.Wall);
        }
    }

    private void UseSelectedResource()
    {
        switch (ResourceManager.instance.ActiveResource)
        {
            case ResourceManager.ResourceE.Pumpkin:
                if (ResourceManager.instance.TryUseResource())
                {
                    Instantiate(pumpkinPrefab,
                        transform.position + transform.forward * 2, Quaternion.identity);
                }
                break;
            case ResourceManager.ResourceE.Wall:
                if (ResourceManager.instance.TryUseResource())
                {
                    Instantiate(wallPrefab,
                        transform.position + transform.forward * 2, Quaternion.identity);
                }
                break;
        }

    }
}
