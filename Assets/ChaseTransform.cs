using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChaseTransform : MonoBehaviour
{
    [SerializeField] NavMeshAgent agent;
    [SerializeField] public Transform target;
    void Update()
    {
        agent.SetDestination(target.position);
    }
}
