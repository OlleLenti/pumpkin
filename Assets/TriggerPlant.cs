using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlant : MonoBehaviour
{
    [SerializeField]
    private Plant plant;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            plant.playerTriggerActive = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            plant.playerTriggerActive = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player")
        {
        }
    }
}
