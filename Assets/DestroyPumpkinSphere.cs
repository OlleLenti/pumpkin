using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPumpkinSphere : MonoBehaviour
{
    public bool DestroyAll = false;
    private List<ExplodablePumpkin> Explodables = new List<ExplodablePumpkin>();

    private void OnTriggerEnter(Collider other)
    {
        var destroyable = other.GetComponent<ExplodablePumpkin>();
        if (destroyable != null)
        {
            Explodables.Add(destroyable);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var destroyable = other.GetComponent<ExplodablePumpkin>();
        if (destroyable != null)
        {
            if (Explodables.Contains(destroyable))
            {
                Explodables.Remove(destroyable);
            }
        }
    }

    private void Update()
    {
        if (DestroyAll)
        {
            DestroyAll = false;

            foreach (var item in Explodables)
            {
                item.Explode();
                ResourceManager.instance.AddResource(ResourceManager.ResourceE.Pumpkin, 1);
            }
            Explodables.Clear();
        }
    }
}
