using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour
{
    public bool playerTriggerActive = false;
    private float age = 20;
    [SerializeField] private float maxAge = 1000f;

    private void Start()
    {
        var growth = 1f / maxAge;
        transform.localScale = Vector3.one * age * growth;
    }

    void Update()
    {
        age++;

        var wantedSize = Mathf.Min(1f, age / maxAge);

        var growth = 1f/maxAge;

        if (playerTriggerActive)
        {
            Debug.Log("playerTriggerActive");
            transform.localScale += Vector3.one * growth;
        }
        else if (transform.localScale.x - wantedSize > 0.001f)
        {
            transform.localScale -= Vector3.one * growth;
        }
        else if (transform.localScale.x - wantedSize < 0.001f)
        {
            transform.localScale += Vector3.one * growth;
        }
    }

}
