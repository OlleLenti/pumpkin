using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resource : MonoBehaviour
{
    [SerializeField] public string Name;
    [SerializeField] public TMPro.TMP_Text TextElement;
    [SerializeField] public int Resources;
    [SerializeField] public Image Image;
    [SerializeField] public Image BackgroundImage;

    void Start()
    {
        TextElement.text = $"{Resources}";
    }

    internal void Use()
    {
        Resources--;
        TextElement.text = $"{Resources}";
    }

    public void Add(int num)
    {
        Resources += num;
        TextElement.text = $"{Resources}";
    }

    internal void SetSelected(bool v)
    {
        BackgroundImage.gameObject.SetActive(v);
    }
}
