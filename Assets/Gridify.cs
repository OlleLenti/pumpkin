using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gridify : MonoBehaviour
{
    public static bool GridifyNow;
    
    void Update()
    {
        if (GridifyNow)
        {
            var roundX = Mathf.Round(transform.position.x);
            var roundZ = Mathf.Round(transform.position.z);

            if (Mathf.Abs(transform.position.x - roundX) < 0.01f && )

            var gridPoints = new List<Vector3> {
                new Vector3(roundX, transform.position.y + 1, roundZ),
            };

            var raycast = Physics.Raycast(gridPoints[0], gridPoints[0] - new Vector3(0, 1, 0), out var a, 10);

            if (raycast)
            {
                transform.position = a.point;
            }
        }
    }
}
