using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    private int Counter;
    [SerializeField] int UpdateFreq = 100;
    void Update()
    {
        Gridify.GridifyNow = Counter++ % UpdateFreq == 0;
    }
}
